package ar.edu.unlam.programacion3.introduccion;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class LeerArchivoYParsearEntrada {

	private final static String fullPath = "resources/entradaParseo.in";
	
	public static void main(String[] args) {
		File file = null;
		FileReader fileReader = null;
		BufferedReader bufferedReader = null;
		
		try {
			file = new File(fullPath);
			fileReader = new FileReader(file);
			bufferedReader = new BufferedReader(fileReader);
			
			String line;
			String[] splitLine;
			double average = 0;
			
			while((line = bufferedReader.readLine()) != null) {
				splitLine = line.split(" ");
				for(int i = 0; i < splitLine.length; i++) {
					average += Integer.parseInt(splitLine[i]);
				}
				System.out.println(average / splitLine.length);
				average = 0;
			}
			
		} catch(FileNotFoundException ex) {
			ex.printStackTrace();
		} catch(IOException ex) {
			ex.printStackTrace();
		} finally {
			try {
				if(bufferedReader != null) {
					bufferedReader.close();
				}
			} catch(IOException ex) {
				ex.printStackTrace();
			}
		}
	}

}
