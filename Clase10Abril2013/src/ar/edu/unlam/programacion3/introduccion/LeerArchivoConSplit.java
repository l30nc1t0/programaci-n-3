package ar.edu.unlam.programacion3.introduccion;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class LeerArchivoConSplit {

	private final static String fullPath = "resources/entradaConSplit.in";

	public static void main(String[] args) {
		File file = null;
		FileReader fileReader = null;
		BufferedReader bufferedReader = null;
		
		try {
			file = new File(fullPath);
			fileReader = new FileReader(file);
			bufferedReader = new BufferedReader(fileReader);
			
			String line;
			String[] splitLine;
			
			while((line = bufferedReader.readLine()) != null) {
				splitLine = line.split(" ");
				for(int i = 0; i < splitLine.length; i++) {
					System.out.println(splitLine[i]);
				}
			}
			
		} catch(FileNotFoundException ex) {
			ex.printStackTrace();
		} catch(IOException ex) {
			ex.printStackTrace();
		} finally {
			try {
				if(bufferedReader != null) {
					bufferedReader.close();
				}
			} catch(IOException ex) {
				ex.printStackTrace();
			}
		}
		
	}

}
