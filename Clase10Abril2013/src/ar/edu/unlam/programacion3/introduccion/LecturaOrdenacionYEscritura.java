package ar.edu.unlam.programacion3.introduccion;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class LecturaOrdenacionYEscritura {

	private static final String path = "resources/loteDePrueba/casos/";
	private static final String[] inputFileNames = {"00_EntradaOrdenada.in",
											   "01_EntradaInvertida.in",
											   "02_EntradaVacia.in",
											   "03_EntradaConNegativos.in",
											   "04_EntradaConDuplicados.in"};
	private static final String[] outputFileNames = {"00_EntradaOrdenada.out",
											   		 "01_EntradaInvertida.out",
											   		 "02_EntradaVacia.out",
											   		 "03_EntradaConNegativos.out",
											   		 "04_EntradaConDuplicados.out"};
	
	public static void main(String[] args) {
		File file = null;
		FileReader fileReader = null;
		BufferedReader bufferedReader = null;
		FileWriter fileWriter = null;
		PrintWriter printWriter = null;
		
		for(int i = 0; i < inputFileNames.length; i++) {
			try {
				file = new File(path + inputFileNames[i]);
				fileReader = new FileReader(file);
				bufferedReader = new BufferedReader(fileReader);
				
				String line = bufferedReader.readLine();
				
				int cantidadNumeros = Integer.parseInt(line);
				int[] numeros = null;
				
				if(cantidadNumeros > 0) {
					numeros = new int[cantidadNumeros];
					
					for(int j = 0; j < cantidadNumeros; j++) {
						numeros[j] = Integer.parseInt(bufferedReader.readLine());
					}
					
					ordenar(numeros);
				}
				
				file = new File(path + outputFileNames[i]);
				fileWriter = new FileWriter(file);
				printWriter = new PrintWriter(fileWriter);
				
				printWriter.println(cantidadNumeros);
				if(cantidadNumeros > 0) {
					for(int j = 0; j < cantidadNumeros - 1; j++) {
						printWriter.println(numeros[j]);
					}
					printWriter.print(numeros[cantidadNumeros - 1]);
				}
								
			} catch(FileNotFoundException ex) {
				ex.printStackTrace();
			} catch(IOException ex) {
				ex.printStackTrace();
			} finally {
				try {
					if(bufferedReader != null) {
						bufferedReader.close();
					}
					if(printWriter != null) {
						printWriter.close();
					}
				} catch(IOException ex) {
					ex.printStackTrace();
				}
			}
		}
	}

	private static void ordenar(int[] numeros) {
		boolean huboIntercambio = true;
		int j = 0;
		int aux;
		
		while(huboIntercambio) {
			huboIntercambio = false;
			j++;
			for(int i = 0; i < numeros.length - j; i++) {
				if(numeros[i] > numeros[i + 1]) {
					aux = numeros[i];
					numeros[i] = numeros[i + 1];
					numeros[i + 1] = aux;
					huboIntercambio = true;
				}
			}
		}
	}

}
