package ar.edu.unlam.programacion3.introduccion;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class EscribirArchivo {

	private final static String fullPath = "resources/salida.out";
	
	public static void main(String[] args) {
		File file = null;
		FileWriter fileWriter = null;
		PrintWriter printWriter = null;
		
		try {
			file = new File(fullPath);
			fileWriter = new FileWriter(file);
			printWriter = new PrintWriter(fileWriter);
			
			printWriter.println("Esto es un archivo de prueba.");
			printWriter.println("Debería aparecer en el archivo tal cual está.");
			printWriter.print("10-Abril-2013.");
			
		} catch(FileNotFoundException ex) {
			ex.printStackTrace();
		} catch(IOException ex) {
			ex.printStackTrace();
		} finally {
			if(printWriter != null) {
				printWriter.close();
			}
		}
	}

}
